from tools import root_dir, nice_json
from flask import Flask
from werkzeug.exceptions import NotFound
import json
import collections
import mysql.connector
import os
#import sqlite3


app = Flask(__name__)

#db = sqlite3.connect('/tmp/example', timeout = 10)

#cur = db.cursor()

#try:
#    cur.execute("CREATE TABLE movies (title VARCHAR(100), rating FLOAT, director VARCHAR(100), id VARCHAR(100))")
#except:
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('The Good Dinosaur', 7.4, 'Peter Sohn', '720d006c-3a57-4b6a-b18f-9b713b073f3c')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('The Martian', 8.2, 'Ridley Scott', 'a8034f44-aee4-44cf-b32c-74cf452aaaae')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('The Night Before', 7.4, 'Jonathan Levine', '96798c08-d19b-4986-a05d-7da856efb697')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('Creed', 8.8, 'Ryan Coogler', '267eedb8-0f5d-42d5-8f43-72426b9fb3e6')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('Victor Frankenstein', 6.4, 'Paul McGuigan', '7daf7208-be4d-4944-a3ae-c1c2f516f3e6')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('The Danish Girl', 5.3, 'Tom Hooper', '276c79ec-a26a-40a6-b3d3-fb242a5947b6')")
#    cur.execute("INSERT INTO movies (title, rating, director, id) VALUES('Spectre', 7.1, 'Sam Mendes', '39ab85e5-5e8e-4dc5-afea-65dc368bd7ab')")
#    db.commit()


@app.route("/", methods=['GET'])
def hello():
    return nice_json({
        "uri": "/",
        "subresource_uris": {
            "movies": "/movies",
            "movie": "/movies/<id>"
        }
    })

@app.route("/movies/<movieid>", methods=['GET'])
def movie_info(movieid):
    db = mysql.connector.connect(
        host="127.0.0.1",
        user=os.environ['SECRET_USERNAME'],
        passwd=os.environ['SECRET_PASSWORD'],
        database="mydb"
    )
    cur = db.cursor()
    cur.execute('SELECT id FROM movies WHERE id = "{}"'.format(movieid))
    data_to_read = cur.fetchall()
    if data_to_read == []:
        db.close()
        raise NotFound

    cur.execute('SELECT * FROM movies WHERE id = "{}"'.format(movieid))
    data_to_read = cur.fetchall()
    db.close()
    rowarray_list = {}
    for row in data_to_read:
        d = collections.OrderedDict()
        d['title'] = row[0]
        d['rating'] = row[1]
        d['director'] = row[2]
        d['id'] = row[3]
        rowarray_list[d['id']] = d

    j = json.dumps(d, indent = 4, sort_keys = True) 
    parsed = json.loads(j)
    parsed["uri"] = "/movies/{}".format(movieid)
    return nice_json(parsed)


@app.route("/movies", methods=['GET'])
def movie_record():
    db = mysql.connector.connect(
        host="127.0.0.1",
        user=os.environ['SECRET_USERNAME'],
        passwd=os.environ['SECRET_PASSWORD'],
        database="mydb"
    )
    cur = db.cursor()
    cur.execute("SELECT * FROM movies")
    data_to_read = cur.fetchall()
    rowarray_list = {}
    for row in data_to_read:
        d = collections.OrderedDict()
        d['title'] = row[0]
        d['rating'] = row[1]
        d['director'] = row[2]
        d['id'] = row[3]
        rowarray_list[d['id']] = d

    j = json.dumps(rowarray_list, indent = 4, sort_keys = True) 
    parsed = json.loads(j)
    return nice_json(parsed)


if __name__ == "__main__":
    app.run(host= "0.0.0.0", port=8080, debug=True)

