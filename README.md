## To make pipeline work:

### Create servie account

```
Create service account in google cloud account with roles:
- Storage Admin
- Kubernetes Engine Developer
```

### Changing gitlab variables

```
Save json file in gitlab's Environment variables
Change JAN_ACCESS to your variable name
Change PROJECT_ID_JAN to your project id
```

### Changes inside .gitlab-ci.yml file

```
- Name of cluster
- Zone
```

### Running pipline 


Make commit on one of dev, master or add other branch name in:

```
only:
  refs:
    - name_of_branch
```

You are also required to create namespace with the name of your branch

```
kubectl create namespace name_of_namespace
```

### If you want to change runner

```
Change "tags:" to match your runner's tags
```

### PROXY requirements

```
credentials.json with permission to SQL Admin role in microservices folder
mysecret.yaml configure with username and password
deploy-demloyment-pods.yaml configure with new 'instance connection name' (found in google sql instance)
```


